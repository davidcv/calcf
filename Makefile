
all: myprog 

myprog: runtime.s.bc myprog.s.bc
	llvm-ld myprog.s.bc runtime.s.bc -o prog.exe

runtime.s:
	clang -c -S -emit-llvm runtime.c

runtime.s.bc: runtime.s
	llvm-as runtime.s

myprog.s.bc: myprog.s
	llvm-as myprog.s

.PHONY: clean

clean:
	rm -rf  *.o *.s.bc runtime.s
