package semantics;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

import parser.ParseException;
import parser.Parser;
import ast.ICalcState;

public class TypeCheckerTestHC {

	private String[] exprs = {
			"decl x=1,k=var(true) in " + "decl f= func y:int => y+x end in "
					+ "decl g= func x:int => x+f(x) end in "
					+ "print(g(2));k:=x end end end",
			"decl x=1,k=true in " + "decl f= func y:int => y+x end in "
					+ "decl g= func x:int => x+f(x) end in "
					+ "print(g(2));k:=false end end end",

			"decl x=1,k=var(2) in " + "decl f= func y:int => y+x end in "
					+ "decl g= func x:int => x+f(x) end in "
					+ "while(!k) do print(g(2)) end end end end",
			"decl x=1,k=var(2) in "
					+ "decl f= func y:int => y+x end in "
					+ "decl g= func x:int => x+f(x) end in "
					+ "if(!k) then print(g(2)) else print(g(3)) end end end end",
			"decl x=1,k=var(false) in "
					+ "decl f= func y:int => y+x end in "
					+ "decl g= func x:int => x+f(x) end in "
					+ "if((3>!k)) then print(g(2)) else print(g(3)) end end end end",
			"decl x=1,k=false in " + "decl f= func y:int => y+k end in "
					+ "decl g= func x:int => x+f(x) end in "
					+ "print(g(2)) end end end",
			"decl x=1,k=false in " + "decl f= func y:int => y-k end in "
					+ "decl g= func x:int => x+f(x) end in "
					+ "print(g(2)) end end end",
			"decl x=1,k=false in " + "decl f= func y:int => y*k end in "
					+ "decl g= func x:int => x+f(x) end in "
					+ "print(g(2)) end end end",
			"decl x=1,k=false in " + "decl f= func y:int => y/k end in "
					+ "decl g= func x:int => x+f(x) end in "
					+ "print(g(2)) end end end",
			"decl x=1 in " + "decl f= func y:int => y+x end in "
					+ "decl g= func x:int => x+f(x) end in "
					+ "print(g(2)) end end end",
			"decl x=1 in " + "decl f= func y:int => y+z end in "
					+ "decl g= func x:int => x+f(x) end in "
					+ "print(g(2)) end end end",
			"decl x=1 in " + "decl f= func y:int => y+x end in "
					+ "decl g= func x:int => x+f(x) end in "
					+ "print(x(2)) end end end",
			"decl x=1 in " + "decl f= func y:int => y+x end in "
					+ "decl g= func x:int => x+f(x) end in "
					+ "print(f(false)) end end end" };

	private TypeCheckerVisitor typeChecker;
	private PipedInputStream convertPipe;
	private PipedOutputStream dataPipe;
	private Parser p;

	public TypeCheckerTestHC() throws IOException {
		typeChecker = new TypeCheckerVisitor();
		convertPipe = new PipedInputStream(2048);
		dataPipe = new PipedOutputStream(convertPipe);
		p = new Parser(convertPipe);
	}

	public void runTestSuite() {
		for (int i = 0; i < exprs.length; i++) {
			try {
				dataPipe.write(exprs[i].getBytes());
				dataPipe.write("\n".getBytes());
				dataPipe.flush();
				@SuppressWarnings("static-access")
				ICalcState prog = p.main();
				prog.accept(typeChecker, null);
			} catch (ParseException e) {
				System.out.print("\nSintaxy error: ");
				System.out.println(e.getMessage());
				System.out.println(exprs[i]);
			} catch (Exception e) {
				System.out.print("\nSintaxy error: ");
				System.out.println(e.getMessage());
				System.out.println(exprs[i]);
			}
		}
	}

	public static void main(String[] args) throws IOException {
		TypeCheckerTestHC tyChekerT = new TypeCheckerTestHC();
		tyChekerT.runTestSuite();
	}
}
