package parser;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

import parser.ParseException;
import parser.Parser;
import ast.ICalcState;

public class ParserTestBase {

	private PipedInputStream convertPipe;
	private PipedOutputStream dataPipe;
	private Parser p;

	public ParserTestBase() throws IOException {
		this.convertPipe = new PipedInputStream(2048);
		try {
			this.dataPipe = new PipedOutputStream(convertPipe);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
		this.p = new Parser(convertPipe);
	}

	@SuppressWarnings("static-access")
	public ICalcState parse(String prog) throws IOException, ParseException {
		dataPipe.write(prog.getBytes());
		dataPipe.write("\n".getBytes());
		dataPipe.flush();
		return p.main();
	}
}
