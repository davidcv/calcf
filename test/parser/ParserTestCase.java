package parser;

import java.io.IOException;
import java.text.ParseException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class ParserTestCase {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	
	@Test(expected = ParseException.class)
	public final void testExprErrDecl() throws IOException, parser.ParseException {
		ParserTestBase parser = new ParserTestBase();
		parser.parse("decl x=3 in end");		
	}



}
