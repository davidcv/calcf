package compiler;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;

import ast.expr.type.*;

import compiler.blocks.CondBlock;
import compiler.blocks.IBlock;
import compiler.blocks.UnCondBlock;
import compiler.dest.IDest;
import compiler.dest.RegIR;

public class CodeLLVM {

	private List<IBlock> prog;

	private int reg_counter;
	private int label_counter;
	private static String HEADER = "@.str = private unnamed_addr constant [5 x i8] c\" %d \\00\", align 1\n"
			+ "\n"
			+ "@.strln = private unnamed_addr constant [2 x i8] c\"\\0A\\00\", align 1\n"
			+ "%struct.var_type = type { i32, %union.anon }\n"
			+ "%union.anon = type { %struct.var_type* }\n"
			+ "%struct.closure_type = type { i8*, void ()* }\n"
			+ "\ndefine i32 @main() nounwind {\n";

	private static String FOOTER = "\nRESULT:\n  ret i32 0\n}\n\n"
			+ "declare i32 @printf(i8*, ...) nounwind\n"
			+ "declare noalias i8* @malloc(i64) nounwind\n"
			+ "declare %struct.var_type* @int_var_create(i32)\n"
			+ "declare %struct.var_type* @var_var_create(%struct.var_type*)\n"
			+ "declare i32 @int_get_var(%struct.var_type*)\n"
			+ "declare %struct.var_type* @var_get_var(%struct.var_type*)\n"
			+ "declare void @int_set_var(%struct.var_type*, i32)\n"
			+ "declare void @var_set_var(%struct.var_type*, %struct.var_type*)\n"
			+ "declare void @free_var(%struct.var_type*)\n"
			+ "declare %struct.closure_type* @closure_create(i8*, void ()*)\n";

	private static String PRINT_CALL = " = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds"
			+ " ([5 x i8]* @.str, i32 0, i32 0), i32 ";
	private static String PRINTLN_CALL = "call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([2 x i8]* @.strln, i32 0, i32 0)) nounwind";

	public CodeLLVM() {

		this.prog = new LinkedList<IBlock>();
		this.reg_counter = 0;
		this.label_counter = 0;
	}

	public int newReg() {
		return reg_counter++;
	}

	public String newLabel() {
		return "BB" + label_counter++;
	}

	public IDest genAnd(IDest src1, IDest src2, String s, String e) {
		UnCondBlock b = new UnCondBlock(s);
		int reg = newReg();
		b.addOp("  %" + reg + " = and i1 " + src1 + " , " + src2);
		b.setToLabel(e);
		prog.add(b);
		RegIR regIR = new RegIR(reg);
		regIR.setType(src1.getType());
		return regIR;
	}

	public IDest genNot(IDest src1, String s, String e) {
		UnCondBlock b = new UnCondBlock(s);
		int reg = newReg();
		b.addOp("  %" + reg + " = xor i1 1 ," + src1);
		b.setEntryLabel(e);
		prog.add(b);
		RegIR regIR = new RegIR(reg);
		regIR.setType(src1.getType());
		return regIR;
	}

	public IDest genGt(IDest src1, IDest src2, String s, String e) {
		UnCondBlock b = new UnCondBlock(s);
		int reg = newReg();
		b.addOp("  %" + reg + " = icmp sge i32 " + src1 + " , " + src2);
		RegIR regIR = new RegIR(reg);
		regIR.setType(src1.getType());
		b.setToLabel(e);
		prog.add(b);
		return regIR;
	}

	public IDest genAdd(IDest src1, IDest src2, String s, String e) {
		int reg = newReg();
		UnCondBlock b = new UnCondBlock(s);
		b.addOp("  %" + reg + " = add i32 " + src1 + " , " + src2);
		RegIR regIR = new RegIR(reg);
		regIR.setType(src1.getType());
		b.setToLabel(e);
		prog.add(b);
		return regIR;
	}

	public IDest genSub(IDest src1, IDest src2, String s, String e) {
		int reg = newReg();
		UnCondBlock b = new UnCondBlock(s);
		b.addOp("  %" + reg + " = sub i32 " + src1 + " , " + src2);
		b.setToLabel(e);
		prog.add(b);
		RegIR regIR = new RegIR(reg);
		regIR.setType(src1.getType());
		return regIR;
	}

	public IDest genMul(IDest src1, IDest src2, String s, String e) {
		int reg = newReg();
		UnCondBlock b = new UnCondBlock(s);
		b.addOp("  %" + reg + " = mul i32 " + src1 + " , " + src2);
		b.setToLabel(e);
		prog.add(b);
		RegIR regIR = new RegIR(reg);
		regIR.setType(src1.getType());
		return regIR;
	}

	public IDest genDiv(IDest src1, IDest src2, String s, String e) {
		int reg = newReg();
		UnCondBlock b = new UnCondBlock(s);
		b.addOp("  %" + reg + " = sdiv i32 " + src1 + " , " + src2);
		b.setToLabel(e);
		prog.add(b);
		RegIR regIR = new RegIR(reg);
		regIR.setType(src1.getType());
		return regIR;
	}

	public IDest genPrint(IDest dest, String s, String e) {
		int reg = newReg();
		UnCondBlock b = new UnCondBlock(s);
		b.addOp("  %" + reg + PRINT_CALL + dest + ")");
		b.setToLabel(e);
		prog.add(b);
		RegIR regIR = new RegIR(reg);
		regIR.setType(dest.getType());
		return regIR;
	}

	public IDest genPrintln(String s, String e) {
		int reg = newReg();
		UnCondBlock b = new UnCondBlock(s);
		b.addOp("  %" + reg + " = "+PRINTLN_CALL);
		b.setToLabel(e);
		prog.add(b);
		RegIR regIR = new RegIR(reg);
		regIR.setType(new UnitT());
		return regIR;
	}

	public IDest genNewVar(IType t, IDest val, String s, String e) {
		int reg = newReg();
		UnCondBlock b = new UnCondBlock(s);
		b.setToLabel(e);
		if (t instanceof IntT) {
			b.addOp("  %" + reg
					+ " = call %struct.var_type* @int_var_create(i32 " + val
					+ ") nounwind ssp ");
		} else if (t instanceof BoolT) {
			b.addOp("  %" + reg
					+ " = call %struct.var_type* @int_var_create(i1 " + val
					+ ") nounwind ssp ");
		} else if (t instanceof RefT) {
			b.addOp("  %"
					+ reg
					+ " = call %struct.var_type* @var_var_create(%struct.var_type* "
					+ val + ") nounwind ssp ");
		}
		b.setToLabel(e);
		prog.add(b);
		RegIR regIR = new RegIR(reg);
		regIR.setType(t);
		return regIR;
	}

	public IDest genSetVar(IType t, IDest dest, IDest val, String s, String e) {
		UnCondBlock b = new UnCondBlock(s);
		b.setToLabel(e);

		if (t instanceof IntT) {
			b.addOp("  call void @int_set_var(%struct.var_type* " + dest
					+ ", i32 " + val + ") nounwind ssp");
		} else if (t instanceof BoolT) {
			b.addOp("  call void @int_set_var(%struct.var_type* " + dest
					+ ", i1 " + val + ") nounwind ssp");
		} else if (t instanceof RefT) {
			b.addOp("  call void @var_set_var(%struct.var_type* " + dest
					+ ", %struct.var_type* " + val + ") nounwind ssp");
		}
		b.setToLabel(e);
		prog.add(b);

		return null;
	}

	public IDest genGetVar(IType t, IDest dest, String s, String e) {
		int reg = newReg();
		UnCondBlock b = new UnCondBlock(s);
		b.setToLabel(e);
		if (t instanceof IntT) {
			b.addOp("  %" + reg + " = call i32 @int_get_var(%struct.var_type* "
					+ dest + ") nounwind ssp ");
		} else if (t instanceof BoolT) {
			b.addOp("  %" + reg + " = call i1 @int_get_var(%struct.var_type* "
					+ dest + ") nounwind ssp ");
		} else if (t instanceof RefT) {
			b.addOp("  %"
					+ reg
					+ " = call %struct.var_type* @int_get_var(%struct.var_type* "
					+ dest + ") nounwind ssp ");
		}
		b.setToLabel(e);
		prog.add(b);
		RegIR regIR = new RegIR(reg);
		regIR.setType(t);
		return regIR;
	}

	public IDest genWhile(IDest cond, String l_c, String l_b, String l_e,
			String endL, String s) {
		CondBlock cb = new CondBlock(l_c);

		cb.setCond(cond);
		cb.setThenLabel(l_b);
		cb.setElseLabel(endL);
		prog.add(cb);

		UnCondBlock uc = new UnCondBlock(l_e);
		uc.setToLabel(s);
		prog.add(uc);
		return cond;
	}

	public IDest genIf(IDest cond, String l_cond, String l_then,
			String l_then_exit, String l_else, String l_else_exit,
			String l_exit, String endL, IDest if_body, IDest if_el) {
		
		CondBlock cd = new CondBlock(l_cond);
		cd.setCond(cond);
		cd.setThenLabel(l_then);
		cd.setElseLabel(l_else);
		prog.add(cd);
		
		UnCondBlock uc_t_ext = new UnCondBlock(l_then_exit);
		uc_t_ext.setToLabel(l_exit);
		prog.add(uc_t_ext);

		UnCondBlock uc_e_ext = new UnCondBlock(l_else_exit);
		uc_e_ext.setToLabel(l_exit);
		prog.add(uc_e_ext);

		int reg = newReg();
		UnCondBlock uc_l_exit = new UnCondBlock(l_exit);
		uc_l_exit.setToLabel(endL);
		uc_l_exit.addOp("  %" + reg + " = phi i32 [" + if_body + " , %"
				+l_then_exit + "], [" + if_el + " , %" + l_else_exit + "]");

		prog.add(uc_l_exit);
		RegIR regIR = new RegIR(reg);
		return regIR;
	}

	public void dump(String fileName) {
		// Create a new Path
		Path newFile = Paths.get(fileName);
		try {
			Files.deleteIfExists(newFile);
			newFile = Files.createFile(newFile);
		} catch (IOException ex) {
			System.out.println("Error creating file");
		}

		// Writing to file
		try (BufferedWriter writer = Files.newBufferedWriter(newFile,
				Charset.defaultCharset())) {
			writer.newLine();
			writer.newLine();
			writer.append(HEADER);
			writer.newLine();
			writer.append(prog.get(0).toString());
			for (int i = 1; i < prog.size(); i++) {
				writer.newLine();
				writer.newLine();
				writer.append(prog.get(i).toString());
			}
			writer.newLine();
			writer.append(FOOTER);

			writer.flush();
			writer.close();

		} catch (IOException exception) {
			System.out.println("Error writing to file");
		}
	}

}
