package compiler.dest;

import ast.expr.type.IType;

public class ConstIR implements IDest {

	private int n;
	private IType t;

	public ConstIR(int n) {
		this.n = n;
	}

	@Override
	public String toString() {
		return "" + n;
	}

	@Override
	public void setType(IType t) {
		this.t = t;
	}

	@Override
	public IType getType() {
		return t;
	}

	@Override
	public String getEndLabel() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setEndLabel(String label) {
		// TODO Auto-generated method stub
		
	}
}
