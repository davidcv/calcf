package compiler.dest;

import ast.expr.type.IType;

public interface IDest {

	public void setType(IType t);

	public IType getType();

	public String getEndLabel();

	public void setEndLabel(String label);
	
}
