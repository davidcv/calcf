package compiler.dest;

import ast.expr.type.IType;

public class RegIR implements IDest {
	private int n;
	private IType t;
	private String end;

	public RegIR(int n) {
		this.n = n;
	}

	@Override
	public String toString() {
		return "%" + n;
	}

	@Override
	public void setType(IType t) {
		this.t = t;
	}

	@Override
	public IType getType() {
		return t;
	}


	@Override
	public String getEndLabel() {
		return end;
	}

	@Override
	public void setEndLabel(String label) {
		end = label;
		
	}

	
}
