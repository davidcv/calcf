package compiler;

import semantics.IEnv;
import ast.IASTExpression;
import ast.expr.*;

public interface IVisitorComp<T> {

	T visit(IASTExpression node, IEnv<T> env, String startL, String endL);

	T visit(ASTDecl decl, IEnv<T> env, String startL, String endL);

	T visit(ASTAssign assign, IEnv<T> env, String startL, String endL);

	T visit(ASTWhile whil, IEnv<T> env, String startL, String endL);

	T visit(ASTIf ifE, IEnv<T> env, String startL, String endL);

	T visit(ASTSeq seq, IEnv<T> env, String startL, String endL);

	T visit(ASTPrint print, IEnv<T> env, String startL, String endL);

	T visit(ASTPrintln println, IEnv<T> env, String startL, String endL);

	T visit(ASTNum number,String startL, String endL);

	T visit(ASTBool bool, String startL, String endL);

	T visit(ASTAdd plus, IEnv<T> env, String startL, String endL);

	T visit(ASTSub sub, IEnv<T> env, String startL, String endL);

	T visit(ASTMult mult, IEnv<T> env, String startL, String endL);

	T visit(ASTDiv div, IEnv<T> env, String startL, String endL);

	T visit(ASTId id, IEnv<T> env, String startL, String endL);

	T visit(ASTNot not, IEnv<T> env, String startL, String endL);

	T visit(ASTDeref deref, IEnv<T> env, String startL, String endL);

	T visit(ASTGt gt, IEnv<T> env, String startL, String endL);

	T visit(ASTVar var, IEnv<T> env, String startL, String endL);

	T visit(ASTAnd and, IEnv<T> env, String startL, String endL);

	T visit(ASTCall call, IEnv<T> env, String startL, String endL);

	T visit(ASTFunc func, IEnv<T> env, String startL, String endL);

}
