package compiler;

import semantics.IEnv;
import ast.IASTExpression;
import ast.expr.*;
import ast.expr.type.BoolT;
import ast.expr.type.IntT;

import compiler.dest.ConstIR;
import compiler.dest.IDest;

public class CompilerVisitor implements IVisitorComp<IDest> {

	private CodeLLVM code;

	public CompilerVisitor() {
		code = new CodeLLVM();
	}

	@Override
	public IDest visit(ASTDecl decl, IEnv<IDest> env, String startL, String endL) {
		if (env == null)
			env = new EnvComp();
		IDest reg = null;
		String m = code.newLabel();

		for (IASTExpression n : decl.eqs) {// this list as one element; cuz the
			reg = ((ASTEq) n).v.accept(this, env, startL, m);
			env.assoc(((ASTEq) n).id, reg);
		}
		if (reg instanceof ConstIR)
			m = startL;
		decl.ins.accept(this, env.beginScope(), m, endL);
		return null;
	}

	public IDest visit(ASTAssign assign, IEnv<IDest> env, String startL,
			String endL) {
		IDest var = assign.e.accept(this, env, startL, endL);
		String m2 = code.newLabel();
		IDest value = assign.v.accept(this, env, startL, m2);
		return code.genSetVar(value.getType(), var, value, m2, endL);
	}

	public IDest visit(ASTWhile whil, IEnv<IDest> env, String startL,
			String endL) {
		String l_cond = code.newLabel();
		String l_body = code.newLabel();
		String l_exit = code.newLabel();

		IDest cond = whil.cond.accept(this, env, startL, l_cond);
		whil.body.accept(this, env, l_body, l_exit);
		return code.genWhile(cond, l_cond, l_body, l_exit, endL, startL);
	}

	public IDest visit(ASTIf ifE, IEnv<IDest> env, String startL, String endL) {
		String l_cond = code.newLabel();
		
		IDest cond = ifE.cond.accept(this, env, startL, l_cond);
		
		if (cond instanceof ConstIR)
			l_cond = startL;
		String l_then = code.newLabel();
		String l_then_exit = code.newLabel();
		IDest if_body = ifE.ifBody.accept(this, env, l_then, l_then_exit);

		String l_else = code.newLabel();
		String l_else_exit = code.newLabel();
		IDest if_el = ifE.elseBody.accept(this, env, l_else, l_else_exit);

		String l_exit = code.newLabel();
		return code.genIf(cond, l_cond, l_then, l_then_exit, l_else,
				l_else_exit, l_exit, endL, if_body, if_el);
	}

	public IDest visit(ASTSeq seq, IEnv<IDest> env, String startL, String endL) {
		String m1 = code.newLabel();
		seq.a.accept(this, env, startL, m1);
		return seq.b.accept(this, env, m1, endL);
	}

	public IDest visit(ASTPrint print, IEnv<IDest> env, String startL,
			String endL) {
		String m1 = code.newLabel();
		IDest accept = print.e.accept(this, env, startL, m1);
		if (accept instanceof ConstIR)
			m1 = startL;
		return code.genPrint(accept, m1, endL);
	}

	public IDest visit(ASTPrintln println, IEnv<IDest> env, String startL,
			String endL) {
		return code.genPrintln(startL, endL);
	}

	public IDest visit(ASTNum number, String startL, String endL) {
		ConstIR c = new ConstIR(number.num);
		c.setType(new IntT());

		return c;
	}

	public IDest visit(ASTBool bool, String startL, String endL) {
		ConstIR c = new ConstIR(bool.b ? 1 : 0);
		BoolT t = new BoolT();
		c.setType(t);
		return c;
	}

	public IDest visit(ASTAdd plus, IEnv<IDest> env, String startL, String endL) {
		String m1 = code.newLabel();
		String m2 = code.newLabel();
		IDest n1 = plus.l.accept(this, env, startL, m1);
		if (n1 instanceof ConstIR)
			m1 = startL;
		IDest n2 = plus.r.accept(this, env, m1, m2);
		if (n2 instanceof ConstIR)
			m2 = m1;

		return code.genAdd(n1, n2, m2, endL);
	}

	public IDest visit(ASTSub sub, IEnv<IDest> env, String startL, String endL) {
		String m1 = code.newLabel();
		String m2 = code.newLabel();
		IDest n1 = sub.l.accept(this, env, startL, m1);
		if (n1 instanceof ConstIR)
			m1 = startL;
		IDest n2 = sub.r.accept(this, env, m1, m2);
		if (n2 instanceof ConstIR)
			m2 = m1;
		return code.genSub(n1, n2, m2, endL);
	}

	public IDest visit(ASTMult mult, IEnv<IDest> env, String startL, String endL) {
		String m1 = code.newLabel();
		String m2 = code.newLabel();
		IDest n1 = mult.l.accept(this, env, startL, m1);
		if (n1 instanceof ConstIR)
			m1 = startL;
		IDest n2 = mult.r.accept(this, env, m1, m2);
		if (n2 instanceof ConstIR)
			m2 = m1;
		return code.genMul(n1, n2, m2, endL);
	}

	public IDest visit(ASTDiv div, IEnv<IDest> env, String startL, String endL) {
		String m1 = code.newLabel();
		String m2 = code.newLabel();
		IDest n1 = div.l.accept(this, env, startL, m1);
		if (n1 instanceof ConstIR)
			m1 = startL;
		IDest n2 = div.r.accept(this, env, m1, m2);
		if (n2 instanceof ConstIR)
			m2 = m1;
		return code.genDiv(n1, n2, m2, endL);
	}

	public IDest visit(ASTId id, IEnv<IDest> env, String startL, String endL) {
		return env.find(id.id);
	}

	public IDest visit(ASTNot not, IEnv<IDest> env, String startL, String endL) {
		String m = code.newLabel();
		IDest n1 = not.e.accept(this, env, startL, m);
		if (n1 instanceof ConstIR)
			m = startL;
		return code.genNot(n1, m, endL);
	}

	public IDest visit(ASTDeref deref, IEnv<IDest> env, String startL,
			String endL) {
		return code.genGetVar(env.find(deref.id).getType(), env.find(deref.id),
				startL, endL);// (IDest)
		// env.find(deref.id).getValue();
	}

	public IDest visit(ASTGt gt, IEnv<IDest> env, String startL, String endL) {
		String m1 = code.newLabel();
		String m2 = code.newLabel();
		IDest n1 = gt.l.accept(this, env, startL, m1);
		if (n1 instanceof ConstIR)
			m1 = startL;
		IDest n2 = gt.r.accept(this, env, m1, m2);
		if (n2 instanceof ConstIR)
			m2 = m1;
		return code.genGt(n1, n2, m2, endL);
	}

	public IDest visit(ASTVar var, IEnv<IDest> env, String startL, String endL) {
		// String m = code.newLabel();
		IDest accept = var.e.accept(this, env, startL, endL);
		return code.genNewVar(accept.getType(), accept, startL, endL);
	}

	public IDest visit(ASTAnd and, IEnv<IDest> env, String startL, String endL) {
		String m1 = code.newLabel();
		String m2 = code.newLabel();
		IDest n1 = and.l.accept(this, env, startL, m1);
		if (n1 instanceof ConstIR)
			m1 = startL;
		IDest n2 = and.r.accept(this, env, m1, m2);
		if (n2 instanceof ConstIR)
			m2 = m1;
		return code.genAnd(n1, n2, m2, endL);
	}

	@Override
	public IDest visit(ASTCall call, IEnv<IDest> env, String startL, String endL) {
		//TODO
		
		// IDest func = call.e.accept(this,env,startL,endL);
		// IDest aa = call.ee.accept(this,env,startL,endL);
		//
		// IDest accept = null;
		// if (func instanceof OpenValue) {
		//
		// String param = ((OpenValue) func).getParam();
		//
		// IEnv<IDest> envv = ((OpenValue) func).getEnv();
		// envv.assoc(param, aa);
		// accept = ((OpenValue) func).getExpr().accept(this, envv);
		//
		// }
		// TODO Call
		return null;// accept;
	}

	@Override
	public IDest visit(ASTFunc func, IEnv<IDest> env, String startL, String endL) {
		// TODO Func
		return null;// new OpenValue(func.e, func.param.id, env);
	}

	public IDest visit(IASTExpression node, IEnv<IDest> env, String startL,
			String endL) {
		if (node == null)
			return null;
		if (node instanceof ASTNum) {
			return visit((ASTNum) node, startL, endL);
		} else if (node instanceof ASTAdd) {
			return visit((ASTAdd) node, env, startL, endL);
		} else if (node instanceof ASTSub) {
			return visit((ASTSub) node, env, startL, endL);
		} else if (node instanceof ASTMult) {
			return visit((ASTMult) node, env, startL, endL);
		} else if (node instanceof ASTDiv) {
			return visit((ASTDiv) node, env, startL, endL);
		} else if (node instanceof ASTId) {
			return visit((ASTId) node, env, startL, endL);
		} else if (node instanceof ASTNot) {
			return visit((ASTNot) node, env, startL, endL);
		} else if (node instanceof ASTDeref) {
			return visit((ASTDeref) node, env, startL, endL);
		} else if (node instanceof ASTGt) {
			return visit((ASTGt) node, env, startL, endL);
		} else if (node instanceof ASTVar) {
			return visit((ASTVar) node, env, startL, endL);
		} else if (node instanceof ASTBool) {
			return visit((ASTBool) node, startL, endL);
		} else if (node instanceof ASTAnd) {
			return visit((ASTAnd) node, env, startL, endL);
		} else if (node instanceof ASTFunc) {
			return visit((ASTFunc) node, env, startL, endL);
		} else if (node instanceof ASTEq) {
			return visit((ASTEq) node, env, startL, endL);
		} else if (node instanceof ASTCall) {
			return visit((ASTCall) node, env, startL, endL);
		} else if (node instanceof ASTDecl) {
			return visit((ASTDecl) node, env, startL, endL);
		} else if (node instanceof ASTAssign) {
			return visit((ASTAssign) node, env, startL, endL);
		} else if (node instanceof ASTIf) {
			return visit((ASTIf) node, env, startL, endL);
		} else if (node instanceof ASTWhile) {
			return visit((ASTWhile) node, env, startL, endL);
		} else if (node instanceof ASTSeq) {
			return visit((ASTSeq) node, env, startL, endL);
		} else if (node instanceof ASTPrint) {
			return visit((ASTPrint) node, env, startL, endL);
		} else if (node instanceof ASTPrintln) {
			return visit((ASTPrintln) node, env, startL, endL);
		}
		return null;
	}

	public CodeLLVM getCode() {
		return this.code;
	}

}
