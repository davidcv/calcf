package compiler.blocks;

import java.util.LinkedList;
import java.util.List;

public abstract class BaseBlock implements IBlock{

	private String entryLabel;
	private List<String> ops;
	
	public BaseBlock(String entryLabel) {
		this.entryLabel = entryLabel;
		ops = new LinkedList<String>();
	}
	
	public String getEntryLabel() {
		return entryLabel;
	}

	public void setEntryLabel(String entryLabel) {
		this.entryLabel = entryLabel;
	}

	public List<String> getOps() {
		return ops;
	}
	
	@Override
	public void addOp(String op) {
		ops.add(op);
	}
	
	
	
	
	
	//TODO Q? how to start with bolck in decl or with if and elses?
	//TODO Code generation will be done from here?? 
	//TODO how does de label manager works??
}
