package compiler.blocks;

public class UnCondBlock extends BaseBlock {
	private String toLabel;

	public UnCondBlock(String entryLabel) {
		super(entryLabel);
		// TODO Auto-generated constructor stub
	}

	public void setToLabel(String label) {
		this.toLabel = label;
	}
	
	public String getToLabel() {
		return this.toLabel;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		//header
		sb.append(getEntryLabel()+":");
		
		for (String s: getOps()) {
			sb.append("\n"+s);
			//body
		}
		//footer
		sb.append( "\n  br label %"+getToLabel());
		return sb.toString();
	}

}
