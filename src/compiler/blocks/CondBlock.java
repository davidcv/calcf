package compiler.blocks;

import compiler.dest.IDest;

public class CondBlock extends BaseBlock {
	private IDest cond;

	private String thenLabel;
	private String elseLabel;

	public CondBlock(String entryLabel) {
		super(entryLabel);
	}

	public IDest getCond() {
		return cond;
	}

	public void setCond(IDest cond) {
		this.cond = cond;
	}

	public String getThenLabel() {
		return thenLabel;
	}

	public void setThenLabel(String thenLabel) {
		this.thenLabel = thenLabel;
	}

	public String getElseLabel() {
		return elseLabel;
	}

	public void setElseLabel(String elseLabel) {
		this.elseLabel = elseLabel;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		// header
		sb.append(getEntryLabel() + ":\n");

		for (String s : getOps()) {
			sb.append(s + "\n");
			// body
		}
		// footer
		sb.append("  br i1 " + cond + ", label %" + thenLabel + ", label %"
				+ elseLabel);
		return sb.toString();
	}
}
