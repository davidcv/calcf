package compiler;

import java.util.HashMap;
import java.util.Map;

import semantics.IEnv;

import compiler.dest.IDest;

public class EnvComp implements IEnv<IDest> {

	private IEnv<IDest> parent;
	private Map<String, IDest> symbolTable;

	public EnvComp() {
		parent = null;
		symbolTable = new HashMap<String, IDest>();
	}

	@Override
	public IEnv<IDest> beginScope() {
		EnvComp child = new EnvComp();
		child.parent = this;
		return child;
	}

	@Override
	public IEnv<IDest> endScope() {
		return this.parent;
	}

	@Override
	public void assoc(String id, IDest v) {
		symbolTable.put(id, v);
	}

	@Override
	public IDest find(String id) {
		if (symbolTable.containsKey(id))
			return symbolTable.get(id);
		else if (parent != null)
			return parent.find(id);
		return null;
	}

	@Override
	public String toString() {
		if (parent == null)
			return symbolTable.toString();
		else
			return parent.toString() + " => " + symbolTable.toString();
	}
}
