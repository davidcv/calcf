package semantics;

import java.util.HashMap;
import java.util.Map;

import semantics.values.IValue;


public class EnvValue implements IEnv<IValue> {

	private IEnv<IValue> parent;
	private Map<String, IValue> symbolTable;

	public EnvValue() {
		parent = null;
		symbolTable = new HashMap<String, IValue>();
	}

	@Override
	public  IEnv<IValue> beginScope() {
		EnvValue child = new EnvValue();
		child.parent = this;
		return child;
	}

	@Override
	public  IEnv<IValue> endScope() {
		return this.parent;
	}

	@Override
	public void assoc(String id, IValue v) {
		symbolTable.put(id, v);
	}

	@Override
	public IValue find(String id) {
		if (symbolTable.containsKey(id))
			return symbolTable.get(id);
		else if (parent != null)
			return parent.find(id);
		return null;
	}
	
	@Override
	public String toString() {
		if(parent== null)
			return symbolTable.toString();
		else 	
			return parent.toString() +" => "+symbolTable.toString();
	}
}
