package semantics.values;


public class RefValue implements IValue {
	
	private IValue v;
	
	public RefValue(IValue v) {
		this.v = v;
	
	}
	
	@Override
	public void set(IValue v) {
		this.v=v;
	}

	@Override
	public IValue getValue() {
		return v;
	}
	
	@Override
	public String toString() {
		return v.toString();
	}
}
