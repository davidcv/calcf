package semantics.values;


public class IntValue implements IValue {

	public final int v;

	public IntValue(int v) {
		this.v = v;
	}

	@Override
	public void set(IValue s) {}

	@Override
	public Integer getValue() {
		return new Integer(v);
	}

	@Override
	public String toString() {
		return v+"";
	}
}
