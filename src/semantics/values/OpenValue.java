package semantics.values;

import semantics.IEnv;
import ast.IASTExpression;

public class OpenValue implements IValue {

	private IASTExpression e;
	protected String param;
	protected IEnv<IValue>  env;

	public OpenValue(IASTExpression e, String param, IEnv<IValue> env) {
		this.setExpr(e);
		this.param = param;
		this.env = env;

	}

	@Override
	public void set(IValue s) {

	}

	@Override
	public Object getValue() {
		return getExpr();
	}

	public String getParam() {
		return param;
	}

	public IEnv<IValue>  getEnv() {
		return env;
	}

	public IASTExpression getExpr() {
		return e;
	}

	public void setExpr(IASTExpression e) {
		this.e = e;
	}	
}
