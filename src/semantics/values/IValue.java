package semantics.values;

public interface IValue  {
	
	
	public  void set(IValue s);
	
	public Object getValue();

}
