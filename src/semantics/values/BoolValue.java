package semantics.values;


public class BoolValue implements IValue  {
	
	private  boolean b;
	
	public BoolValue(boolean b) {
		this.b = b;
	}

	@Override
	public void set(IValue s) {	}

	@Override
	public Boolean getValue() {
		return new Boolean(b);
	}
	
	@Override
	public String toString() {
		return b+"";
	}
}
