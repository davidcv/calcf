package semantics;

import semantics.values.*;
import ast.IASTExpression;
import ast.IVisitor;
import ast.expr.*;

public class EvalVisitorFunc implements IVisitor<IValue> {

	@Override
	public IValue visit(ASTDecl decl, IEnv<IValue> env) throws Exception  {
		if (env == null)
			env = new EnvValue();
		//TODO do we real want decl id := var()
		for (IASTExpression n : decl.eqs) {
				env.assoc(((ASTEq) n).id, ((ASTEq) n).v.accept(this, env));
		}
		return decl.ins.accept(this, env.beginScope());
	}

	@Override
	public IValue visit(ASTAssign assign, IEnv<IValue> env) throws Exception  {
		IValue value = assign.e.accept(this, env);
		value.set(assign.v.accept(this, env));
		return value;
	}

	@Override
	public IValue visit(ASTWhile whil, IEnv<IValue> env) throws Exception  {
		Boolean cond = (Boolean) whil.cond.accept(this, env).getValue();
		while (cond) {
			whil.body.accept(this, env);
			cond = (Boolean) whil.cond.accept(this, env).getValue();
		}
		return whil.cond.accept(this, env);
	}

	@Override
	public IValue visit(ASTIf ifE, IEnv<IValue> env) throws Exception  {
		Boolean cond = (Boolean) ifE.cond.accept(this, env).getValue();
		if (cond) {
			return ifE.ifBody.accept(this, env);
		} else {
			return ifE.elseBody.accept(this, env);
		}
	}

	@Override
	public IValue visit(ASTSeq seq, IEnv<IValue> env) throws Exception {
		seq.a.accept(this, env);
		IValue res = seq.b.accept(this, env);
		return res;
	}

	@Override
	public IValue visit(ASTPrint print, IEnv<IValue> env) throws Exception {
		IValue res = print.e.accept(this, env);
		System.out.print(" "+res+" ");
		return res;
	}

	@Override
	public IValue visit(ASTPrintln println, IEnv<IValue> env) {
		System.out.println();
		return new BoolValue(true) ;
	}

	@Override
	public IValue visit(ASTNum number) {
		return new IntValue(number.num);
	}

	@Override
	public IValue visit(ASTBool bool) {
		return new BoolValue(bool.b);
	}

	@Override
	public IValue visit(ASTAdd plus, IEnv<IValue> env) throws Exception  {
		Integer n1 = (Integer) plus.l.accept(this, env).getValue();
		Integer n2 = (Integer) plus.r.accept(this, env).getValue();
		return new IntValue(n1 + n2);
	}

	@Override
	public IValue visit(ASTSub sub, IEnv<IValue> env) throws Exception  {
		Integer n1 = (Integer) sub.l.accept(this, env).getValue();
		Integer n2 = (Integer) sub.r.accept(this, env).getValue();
		return new IntValue(n1 - n2);
	}

	@Override
	public IValue visit(ASTMult mult, IEnv<IValue> env) throws Exception  {
		Integer n1 = (Integer) mult.l.accept(this, env).getValue();
		Integer n2 = (Integer) mult.r.accept(this, env).getValue();
		return new IntValue(n1 * n2);
	}

	@Override
	public IValue visit(ASTDiv div, IEnv<IValue> env) throws Exception  {
		Integer n1 = (Integer) div.l.accept(this, env).getValue();
		Integer n2 = (Integer) div.r.accept(this, env).getValue();
		return new IntValue(n1 / n2);
	}

	@Override
	public IValue visit(ASTId id, IEnv<IValue> env) {
		return env.find(id.id);
	}

	@Override
	public IValue visit(ASTNot not, IEnv<IValue> env) throws Exception  {
		Boolean b = (Boolean) not.e.accept(this, env).getValue();
		return new BoolValue(b);
	}

	@Override
	public IValue visit(ASTDeref deref, IEnv<IValue> env) {
		return (IValue) env.find(deref.id).getValue();
	}

	@Override
	public IValue visit(ASTGt gt, IEnv<IValue> env) throws Exception  {
		Integer n1 = (Integer) gt.l.accept(this, env).getValue();
		Integer n2 = (Integer) gt.r.accept(this, env).getValue();
		return new BoolValue(n1 > n2);
	}

	@Override
	public IValue visit(ASTVar var, IEnv<IValue> env) throws Exception  {
		return new RefValue(var.e.accept(this, env));
	}

	@Override
	public IValue visit(ASTAnd and, IEnv<IValue> env) throws Exception  {
		Boolean b1 = (Boolean) and.l.accept(this, env).getValue();
		Boolean b2 = (Boolean) and.r.accept(this, env).getValue();
		return new BoolValue(b1 & b2);
	}

	@Override
	public IValue visit(ASTCall call, IEnv<IValue> env) throws Exception  {
		IValue func = call.e.accept(this, env);
		IValue aa = call.ee.accept(this, env);
		
		IValue accept = null;
		if (func instanceof OpenValue) {
			
			String param = ((OpenValue) func).getParam();
			
			IEnv<IValue> envv = ((OpenValue) func).getEnv();
			envv.assoc(param, aa);
			accept = ((OpenValue) func).getExpr().accept(this, envv);

		}
		return accept;
	}

	@Override
	public IValue visit(ASTFunc func, IEnv<IValue> env) {
		return new OpenValue(func.e, func.param.id, env);
	}

	@Override
	public IValue visit(IASTExpression node, IEnv<IValue> env) throws Exception  {
		if (node == null)
			return null;
		if (node instanceof ASTNum) {
			return visit((ASTNum) node);
		} else if (node instanceof ASTAdd) {
			return visit((ASTAdd) node, env);
		} else if (node instanceof ASTSub) {
			return visit((ASTSub) node, env);
		} else if (node instanceof ASTMult) {
			return visit((ASTMult) node, env);
		} else if (node instanceof ASTDiv) {
			return visit((ASTDiv) node, env);
		} else if (node instanceof ASTId) {
			return visit((ASTId) node, env);
		} else if (node instanceof ASTNot) {
			return visit((ASTNot) node, env);
		} else if (node instanceof ASTDeref) {
			return visit((ASTDeref) node, env);
		} else if (node instanceof ASTGt) {
			return visit((ASTGt) node, env);
		} else if (node instanceof ASTVar) {
			return visit((ASTVar) node, env);
		} else if (node instanceof ASTBool) {
			return visit((ASTBool) node);
		} else if (node instanceof ASTAnd) {
			return visit((ASTAnd) node, env);
		} else if (node instanceof ASTFunc) {
			return visit((ASTFunc) node, env);
		} else if (node instanceof ASTEq) {
			return visit((ASTEq) node, env);
		} else if (node instanceof ASTCall) {
			return visit((ASTCall) node, env);
		} else if (node instanceof ASTDecl) {
			return visit((ASTDecl) node, env);
		} else if (node instanceof ASTAssign) {
			return visit((ASTAssign) node, env);
		} else if (node instanceof ASTIf) {
			return visit((ASTIf) node, env);
		} else if (node instanceof ASTWhile) {
			return visit((ASTWhile) node, env);
		} else if (node instanceof ASTSeq) {
			return visit((ASTSeq) node, env);
		} else if (node instanceof ASTPrint) {
			return visit((ASTPrint) node, env);
		} else if (node instanceof ASTPrintln) {
			return visit((ASTPrintln) node, env);
		}
		return null;
		}
	
}
