package semantics;

import java.util.HashMap;
import java.util.Map;

import ast.expr.type.IType;

public class EnvIType implements IEnv<IType> {

	private IEnv<IType> parent;
	private Map<String, IType> symbolTable;

	public EnvIType() {
		parent = null;
		symbolTable = new HashMap<String, IType>();
	}

	@Override
	public IEnv<IType> beginScope() {
		EnvIType child = new EnvIType();
		child.parent = this;
		return child;
	}

	@Override
	public IEnv<IType> endScope() {
		return this.parent;
	}

	@Override
	public void assoc(String id, IType v) {
		symbolTable.put(id, v);
	}

	@Override
	public IType find(String id) {
		if (symbolTable.containsKey(id))
			return symbolTable.get(id);
		else if (parent != null)
			return parent.find(id);
		return null;
	}

	@Override
	public String toString() {
		if (parent == null)
			return symbolTable.toString();
		else
			return parent.toString() + " => " + symbolTable.toString();
	}

}
