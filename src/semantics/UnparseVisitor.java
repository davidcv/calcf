package semantics;

import ast.IASTExpression;
import ast.IVisitor;
import ast.expr.*;

public class UnparseVisitor implements IVisitor<String> {

	@Override
	public String visit(ASTAdd plus, IEnv<String> env) throws Exception {
		return "Plus(" + plus.l.accept(this, env) + ","
				+ plus.r.accept(this, env) + ")";
	}

	@Override
	public String visit(ASTAnd node, IEnv<String> env) throws Exception {
		return "And(" + node.l + "," + node.r + ")";
	}

	@Override
	public String visit(ASTAssign assign, IEnv<String> env) throws Exception {
		return assign.e.accept(this, env) + ":=" + assign.v.accept(this, env);
	}

	@Override
	public String visit(ASTBool bool) {
		return "" + bool.b;
	}

	@Override
	public String visit(ASTCall call, IEnv<String> env) throws Exception {
		return call.e.accept(this, env) + " (" + call.ee.accept(this, env)
				+ ")";
	}

	@Override
	public String visit(ASTDecl decl, IEnv<String> env) throws Exception {
		StringBuilder sb = new StringBuilder(" Decl ");
		for (IASTExpression n : decl.eqs)
			sb.append(n.accept(this, env));
		sb.append(" in ");
		sb.append(decl.ins.accept(this, env) + " end");
		return sb.toString();
	}

	@Override
	public String visit(ASTDeref deref, IEnv<String> env) throws Exception {
		return " !" + deref.id;
	}

	@Override
	public String visit(ASTDiv div, IEnv<String> env) throws Exception {
		return "Div(" + div.l.accept(this, env) + "," + div.r.accept(this, env)
				+ ")";
	}

	public String visit(ASTEq eq, IEnv<String> env) throws Exception {
		return " id(" + eq.id + ")=" + eq.v.accept(this, env);
	}

	@Override
	public String visit(ASTFunc func, IEnv<String> env) throws Exception {
		StringBuilder sb = new StringBuilder(" func ");
		sb.append(func.param.accept(this, env));
		sb.append(" => ");
		sb.append(func.e.accept(this, env) + " end");
		return sb.toString();
	}

	@Override
	public String visit(ASTGt gt, IEnv<String> env) throws Exception {
		return "Gt(" + gt.l.accept(this, env) + "," + gt.r.accept(this, env)
				+ " )";
	}

	@Override
	public String visit(ASTId id, IEnv<String> env) throws Exception {
		return " id(" + id.id + ")";
	}

	@Override
	public String visit(ASTIf ifE, IEnv<String> env) throws Exception {
		return "if(" + ifE.cond.accept(this, env) + ") { "
				+ ifE.ifBody.accept(this, env) + " } else { "
				+ ifE.elseBody.accept(this, env) + " } ";
	}

	@Override
	public String visit(ASTMult mult, IEnv<String> env) throws Exception {
		return "Mult(" + mult.l.accept(this, env) + ","
				+ mult.r.accept(this, env) + ")";
	}

	@Override
	public String visit(ASTNot not, IEnv<String> env) throws Exception {
		return " not(" + not.e + ")";
	}

	@Override
	public String visit(ASTNum number) {
		return "" + number.num;
	}

	@Override
	public String visit(ASTPrint print, IEnv<String> env) throws Exception {
		return "Print(" + print.e.accept(this, env) + ")";
	}

	@Override
	public String visit(ASTPrintln println, IEnv<String> env) throws Exception {
		return "Println()";
	}

	@Override
	public String visit(ASTSeq seq, IEnv<String> env) throws Exception {
		return seq.a.accept(this, env) + " ; " + seq.b.accept(this, env);
	}

	@Override
	public String visit(ASTSub sub, IEnv<String> env) throws Exception {
		return "Sub(" + sub.l.accept(this, env) + "," + sub.r.accept(this, env)
				+ ")";
	}

	@Override
	public String visit(ASTVar var, IEnv<String> env) throws Exception {
		return "Var(" + var.e.accept(this, env) + ")";
	}

	@Override
	public String visit(ASTWhile whil, IEnv<String> env) throws Exception {
		return "while(" + whil.cond.accept(this, env) + ") { "
				+ whil.body.accept(this, env) + " } ";
	}

	@Override
	public String visit(IASTExpression node, IEnv<String> env) throws Exception {
		if (node == null)
			return null;
		if (node instanceof ASTNum) {
			return visit((ASTNum) node);
		} else if (node instanceof ASTAdd) {
			return visit((ASTAdd) node, env);
		} else if (node instanceof ASTSub) {
			return visit((ASTSub) node, env);
		} else if (node instanceof ASTMult) {
			return visit((ASTMult) node, env);
		} else if (node instanceof ASTDiv) {
			return visit((ASTDiv) node, env);
		} else if (node instanceof ASTId) {
			return visit((ASTId) node, env);
		} else if (node instanceof ASTNot) {
			return visit((ASTNot) node, env);
		} else if (node instanceof ASTDeref) {
			return visit((ASTDeref) node, env);
		} else if (node instanceof ASTGt) {
			return visit((ASTGt) node, env);
		} else if (node instanceof ASTVar) {
			return visit((ASTVar) node, env);
		} else if (node instanceof ASTBool) {
			return visit((ASTBool) node);
		} else if (node instanceof ASTAnd) {
			return visit((ASTAnd) node, env);
		} else if (node instanceof ASTFunc) {
			return visit((ASTFunc) node, env);
		} else if (node instanceof ASTEq) {
			return visit((ASTEq) node, env);
		} else if (node instanceof ASTCall) {
			return visit((ASTCall) node, env);
		} else if (node instanceof ASTDecl) {
			return visit((ASTDecl) node, env);
		} else if (node instanceof ASTAssign) {
			return visit((ASTAssign) node, env);
		} else if (node instanceof ASTIf) {
			return visit((ASTIf) node, env);
		} else if (node instanceof ASTWhile) {
			return visit((ASTWhile) node, env);
		} else if (node instanceof ASTSeq) {
			return visit((ASTSeq) node, env);
		} else if (node instanceof ASTPrint) {
			return visit((ASTPrint) node, env);
		} else if (node instanceof ASTPrintln) {
			return visit((ASTPrintln) node, env);
		}
		return null;
	}

}
