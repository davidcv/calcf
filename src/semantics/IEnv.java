package semantics;


public interface IEnv<T> {

	IEnv<T> beginScope();

	IEnv<T> endScope();

	void assoc(String id, T v);

	T find(String id);
}
