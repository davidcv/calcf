package semantics;

import ast.IASTExpression;
import ast.IVisitor;
import ast.expr.*;
import ast.expr.type.*;

public class TypeCheckerVisitor implements IVisitor<IType> {

	@Override
	public IType visit(ASTDecl decl, IEnv<IType> env) throws Exception {
		if (env == null)
			env = new EnvIType();

		for (IASTExpression n : decl.eqs) {
			n.accept(this, env);
		}

		return decl.ins.accept(this, env.beginScope());
	}

	@Override
	public IType visit(ASTAssign assign, IEnv<IType> env) throws Exception {
		IType t1 = assign.e.accept(this, env);
		IType t2 = assign.v.accept(this, env);
		if (!(t1 instanceof RefT))
			throw new Exception("Assigning a non-reference value.");
		if ((((RefT) t1).t.getClass()!=t2.getClass()))
			throw new Exception("Type mismatch on assignment.");
		return t2;
	}

	public IType visit(ASTEq eq, IEnv<IType> env) throws Exception {
		IType type = eq.v.accept(this, env);
		env.assoc(eq.id, type);
		eq.setType(type);
		return eq.getType();
	}

	@Override
	public IType visit(ASTWhile whil, IEnv<IType> env) throws Exception {
		IType t_cond = whil.cond.accept(this, env);
		whil.body.accept(this, env);
		if (!(t_cond instanceof BoolT))
			throw new Exception("Not a Bool value in while condition.");
		return t_cond;
	}

	@Override
	public IType visit(ASTIf ifE, IEnv<IType> env) throws Exception {
		IType t_cond = ifE.cond.accept(this, env);
		//TODO do we real want to return ifBody type??
		ifE.ifBody.accept(this, env);
		ifE.elseBody.accept(this, env);
		if (!(t_cond instanceof BoolT))
			throw new Exception("Not a Bool value in if condition.");
		return t_cond;
	}

	@Override
	public IType visit(ASTSeq seq, IEnv<IType> env) throws Exception {
		seq.a.accept(this, env);
		IType type = seq.b.accept(this, env);
		return type;
	}

	@Override
	public IType visit(ASTPrint print, IEnv<IType> env) throws Exception {
		IType type = print.e.accept(this, env);
		print.setType(type);
		return type;
	}

	@Override
	public IType visit(ASTPrintln println, IEnv<IType> env) throws Exception {
		return new BoolT();
	}

	@Override
	public IType visit(ASTNum number) {
		return new IntT();
	}

	@Override
	public IType visit(ASTBool bool) {
		return new BoolT();
	}

	@Override
	public IType visit(ASTAdd plus, IEnv<IType> env) throws Exception {
		IType t1 = plus.l.accept(this, env);
		IType t2 = plus.r.accept(this, env);
		if (!(t2 instanceof IntT) || !(t1 instanceof IntT))
			throw new Exception("Type mismatch in Add Operation");

		plus.l.setType(t1);
		plus.r.setType(t2);
		plus.setType(t1);
		return t1;
	}

	@Override
	public IType visit(ASTSub sub, IEnv<IType> env) throws Exception {
		IType t1 = sub.l.accept(this, env);
		IType t2 = sub.r.accept(this, env);
		if (!(t2 instanceof IntT) || !(t1 instanceof IntT))
			throw new Exception("Type mismatch in Sub Operation");

		sub.l.setType(t1);
		sub.r.setType(t2);
		sub.setType(t1);
		return t1;
	}

	@Override
	public IType visit(ASTMult mult, IEnv<IType> env) throws Exception {
		IType t1 = mult.l.accept(this, env);
		IType t2 = mult.r.accept(this, env);
		if (!(t2 instanceof IntT) || !(t1 instanceof IntT))
			throw new Exception("Type mismatch in Mult Operation");
		mult.l.setType(t1);
		mult.r.setType(t2);
		mult.setType(t1);
		return t1;
	}

	@Override
	public IType visit(ASTDiv div, IEnv<IType> env) throws Exception {
		IType t1 = div.l.accept(this, env);
		div.l.setType(t1);
		IType t2 = div.r.accept(this, env);
		if (!(t2 instanceof IntT)|| !(t1 instanceof IntT))
			throw new Exception("Type mismatch Div Operation");
		div.l.setType(t1);
		div.r.setType(t2);
		div.setType(t1);
		return t1;
	}

	@Override
	public IType visit(ASTId id, IEnv<IType> env) throws Exception {
		IType type = env.find(id.id);
		if (type == null)
			throw new Exception("Identifier " + id.id + " not found.");
		id.setType(type);
		return type;
	}

	@Override
	public IType visit(ASTNot not, IEnv<IType> env) throws Exception {
		IType t = not.e.accept(this, env);
		if (t instanceof BoolT)
			throw new Exception("Type mismatch in Not operation");
		not.setType(t);
		return t;
	}

	@Override
	public IType visit(ASTDeref deref, IEnv<IType> env) throws Exception {
		IType t = env.find(deref.id);
		if (t == null)
			throw new Exception("Identifier " + deref.id + " not found.");
		if (!(t instanceof RefT))
			throw new Exception("Dereferencing a non-reference value.");
		deref.setType(((RefT) t).t);
		return deref.getType();
	}

	@Override
	public IType visit(ASTGt gt, IEnv<IType> env) throws Exception {
		IType t1 = gt.l.accept(this, env);
		gt.l.setType(t1);
		IType t2 = gt.r.accept(this, env);
		if (t1.getClass()!=t2.getClass() || (t1 instanceof BoolT))
			throw new Exception("Type mismatch in comparation operation");
		gt.l.setType(t1);
		gt.r.setType(t2);
		gt.setType(t1);
		return new BoolT();
	}

	@Override
	public IType visit(ASTVar var, IEnv<IType> env) throws Exception {
		IType type = var.e.accept(this, env);
		var.setType(new RefT(type));
		return var.getType();
	}

	@Override
	public IType visit(ASTAnd and, IEnv<IType> env) throws Exception {
		IType t1 = and.l.accept(this, env);
		and.l.setType(t1);
		IType t2 = and.r.accept(this, env);
		if (!t1.equals(t2) || !(t1 instanceof BoolT))
			throw new Exception(
					"Type mismatch in and operation");
		and.l.setType(t1);
		and.r.setType(t2);
		and.setType(t1);
		return t1;
	}

	@Override
	public IType visit(ASTCall call, IEnv<IType> env) throws Exception {
		IType t_f = call.e.accept(this, env);
		IType t_a = call.ee.accept(this, env);
		if ( !(t_f instanceof FunT))
			throw new Exception("Not a function type value.");
		if ( !((FunT)t_f).t1.getClass().equals(t_a.getClass()))
			throw new Exception("Type mismatch in param and return type.");
		return ((FunT)t_f).t2;
	}

	@Override
	public IType visit(ASTFunc func, IEnv<IType> env) throws Exception {
		env.assoc(func.param.id,func.param.getType());
		IType type = func.e.accept(this, env);
		func.setType(new FunT(func.param.getType(), type));
		return func.getType();
	}

	public IType visit(IASTExpression node, IEnv<IType> env) throws Exception {
		if (node == null)
			return null;
		if (node instanceof ASTNum) {
			return visit((ASTNum) node);
		} else if (node instanceof ASTAdd) {
			return visit((ASTAdd) node, env);
		} else if (node instanceof ASTSub) {
			return visit((ASTSub) node, env);
		} else if (node instanceof ASTMult) {
			return visit((ASTMult) node, env);
		} else if (node instanceof ASTDiv) {
			return visit((ASTDiv) node, env);
		} else if (node instanceof ASTId) {
			return visit((ASTId) node, env);
		} else if (node instanceof ASTNot) {
			return visit((ASTNot) node, env);
		} else if (node instanceof ASTDeref) {
			return visit((ASTDeref) node, env);
		} else if (node instanceof ASTGt) {
			return visit((ASTGt) node, env);
		} else if (node instanceof ASTVar) {
			return visit((ASTVar) node, env);
		} else if (node instanceof ASTBool) {
			return visit((ASTBool) node);
		} else if (node instanceof ASTAnd) {
			return visit((ASTAnd) node, env);
		} else if (node instanceof ASTFunc) {
			return visit((ASTFunc) node, env);
		} else if (node instanceof ASTEq) {
			return visit((ASTEq) node, env);
		} else if (node instanceof ASTCall) {
			return visit((ASTCall) node, env);
		} else if (node instanceof ASTDecl) {
			return visit((ASTDecl) node, env);
		} else if (node instanceof ASTAssign) {
			return visit((ASTAssign) node, env);
		} else if (node instanceof ASTIf) {
			return visit((ASTIf) node, env);
		} else if (node instanceof ASTWhile) {
			return visit((ASTWhile) node, env);
		} else if (node instanceof ASTSeq) {
			return visit((ASTSeq) node, env);
		} else if (node instanceof ASTPrint) {
			return visit((ASTPrint) node, env);
		} else if (node instanceof ASTPrintln) {
			return visit((ASTPrintln) node, env);
		}
		return null;
	}
}
