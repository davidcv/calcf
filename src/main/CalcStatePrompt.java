package main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;

import parser.ParseException;
import parser.Parser;
import parser.ParserTestBase;
import semantics.EvalVisitorFunc;
import semantics.TypeCheckerVisitor;
import ast.ICalcState;

import compiler.CompilerVisitor;

public class CalcStatePrompt {
	public static void main(String args[]) throws ParseException {

		String defaultAsFile = "myprog.s";
		String finName = "";
		// TODO t has been add after the delivery deadline
		if (args.length == 0) {
			@SuppressWarnings("unused")
			Parser parser = new Parser(System.in);
			while (true) {
				System.out.print("CalcState > ");
				try {
					Parser.enable_tracing();
					ICalcState prog = Parser.main();
					// System.out.println("Ok:  "+ prog.accept(new
					// UnparseVisitor(), null));

					prog.accept(new TypeCheckerVisitor(), null);
					prog.accept(new EvalVisitorFunc(), null);

					CompilerVisitor comp = new CompilerVisitor();
					// Set Entry block
					prog.accept(comp, null, "Entry", "RESULT");
					comp.getCode().dump(defaultAsFile);
				} catch (Error e) {
					System.out.println("Parsing error");
					System.out.println(e.getMessage());
					break;
				} catch (Exception e) {
					System.out.println("Parsing error");
//					e.printStackTrace();
					System.err.println(e.getMessage());
					break;
				}
			}
		} else if (args.length >= 2) {
			if (args[0].equals("-c")) {
				finName = args[1];
				Path path = FileSystems.getDefault().getPath(".", finName);
				InputStream in;
				try {
					in = Files.newInputStream(path);
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(in));

					List<String> lines = new LinkedList<String>();
					String line = null;
					while ((line = reader.readLine()) != null)
						lines.add(line);
					ParserTestBase pp = new ParserTestBase();
					if (args.length >= 4) {
						for (int i = 0; i < lines.size(); i++) {
							ICalcState prog = pp.parse(lines.get(i));
							prog.accept(new TypeCheckerVisitor(), null);
							prog.accept(new EvalVisitorFunc(), null);

							CompilerVisitor comp = new CompilerVisitor();
							// Set Entry block
							prog.accept(comp, null, "Entry", "RESULT");
						}
					}
				} catch (IOException e) {
					System.out.println(e.getMessage());
					System.exit(0);
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
			}
		}
	}
}
