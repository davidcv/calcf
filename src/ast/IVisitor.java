package ast;

import semantics.IEnv;
import ast.expr.*;

public interface IVisitor<T> {

	T visit(IASTExpression node, IEnv<T> env) throws Exception ;

	T visit(ASTDecl decl, IEnv<T> env) throws Exception  ;

	T visit(ASTAssign assign, IEnv<T> env) throws Exception ;

	T visit(ASTWhile whil, IEnv<T> env) throws Exception ;

	T visit(ASTIf ifE, IEnv<T> env) throws Exception ;

	T visit(ASTSeq seq, IEnv<T> env) throws Exception ;

	T visit(ASTPrint print, IEnv<T> env) throws Exception ;
	

	T visit(ASTPrintln println, IEnv<T> env) throws Exception ;

	T visit(ASTNum number) ;

	T visit(ASTBool bool) ;

	T visit(ASTAdd plus, IEnv<T> env) throws Exception ;

	T visit(ASTSub sub, IEnv<T> env) throws Exception ;

	T visit(ASTMult mult, IEnv<T> env) throws Exception ;

	T visit(ASTDiv div, IEnv<T> env) throws Exception ;

	T visit(ASTId id, IEnv<T> env) throws Exception ;

	T visit(ASTNot not, IEnv<T> env) throws Exception ;

	T visit(ASTDeref deref, IEnv<T> env) throws Exception ;

	T visit(ASTGt gt, IEnv<T> env) throws Exception ;

	T visit(ASTVar var, IEnv<T> env) throws Exception ;

	T visit(ASTAnd and, IEnv<T> env) throws Exception ;
		
	T visit(ASTCall call, IEnv<T> env) throws Exception ;
	
	T visit(ASTFunc func, IEnv<T> env) throws Exception ;

}
