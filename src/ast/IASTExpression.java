package ast;

import ast.expr.type.IType;


public interface IASTExpression extends ICalcState{

	public void setType(IType t);
	
	public IType getType();
	
}
