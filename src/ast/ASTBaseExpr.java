package ast;

import semantics.IEnv;
import ast.expr.type.IType;

import compiler.IVisitorComp;

public abstract class ASTBaseExpr implements IASTExpression {

	private IType t;

	public <T> T accept(IVisitor<T> visitor, IEnv<T> env) throws Exception {
		return visitor.visit(this, env);
	}
	
	public <T> T accept(IVisitorComp<T> visitor, IEnv<T> env, String startL, String endL) {
		return visitor.visit(this, env,startL,endL);
	}
	
	
	public void setType(IType t) {
		this.t = t;
	}

	public IType getType() {
		return t;
	}
}
