package ast;

import compiler.IVisitorComp;

import semantics.IEnv;

public interface ICalcState {
	public <T> T accept(IVisitor<T> visitor, IEnv<T> env) throws Exception ;
	
	public <T> T accept(IVisitorComp<T> visitor, IEnv<T> env, String startL, String endL);
}
