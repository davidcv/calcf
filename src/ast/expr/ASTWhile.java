package ast.expr;

import ast.ASTBaseExpr;
import ast.IASTExpression;

public class ASTWhile extends ASTBaseExpr {

	public final IASTExpression cond;
	public final IASTExpression body;

	public ASTWhile(IASTExpression cond, IASTExpression body) {
		this.cond = cond;
		this.body = body;
	}

}
