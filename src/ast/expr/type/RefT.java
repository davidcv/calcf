package ast.expr.type;

public class RefT implements IType {
	
	public final IType t;

	public RefT(IType t) {
		this.t = t;
	
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((t == null) ? 0 : t.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof RefT)) {
			return false;
		}
		RefT other = (RefT) obj;
		if (t == null) {
			if (other.t != null) {
				return false;
			}
		} else if (!t.equals(other.t)) {
			return false;
		}
		return true;
	}
	
	@Override
	public String toString() {
		return "Ref ( "+t+" )";
	}
}
