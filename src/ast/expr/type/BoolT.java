package ast.expr.type;

public class BoolT implements IType{
	private boolean isPrimitive = false; 
	@Override
	public String toString() {
		return "BoolT";
	}
	
	public void setPrimitiveType(){
		isPrimitive=true;
	}
	public boolean isPrimitive() {
		return isPrimitive;
	}
}
