package ast.expr.type;

public class FunT implements IType {
	
	public final IType t1, t2;
	
	public FunT(IType t1, IType t2) {
		this.t1 = t1;
		this.t2 = t2;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof FunT)) {
			return false;
		}
		FunT other = (FunT) obj;
		if (t1 == null) {
			if (other.t1 != null) {
				return false;
			}
		} else if (!t1.equals(other.t1)) {
			return false;
		}
		if (t2 == null) {
			if (other.t2 != null) {
				return false;
			}
		} else if (!t2.equals(other.t2)) {
			return false;
		}
		return true;
	}
	
	@Override
	public String toString() {
		return "Func ( "+t1+","+t2+" )";
	}
	

}
