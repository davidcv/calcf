package ast.expr;

import java.util.List;

import ast.ASTBaseExpr;
import ast.IASTExpression;

public class ASTDecl  extends ASTBaseExpr {

	public final IASTExpression ins;
	public final List<IASTExpression> eqs;
	public ASTDecl(List<IASTExpression> eqs, IASTExpression com) {
		this.eqs = eqs;
		this.ins = com;
	}
}
