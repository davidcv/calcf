package ast.expr;

import ast.ASTBaseExpr;
import ast.IASTExpression;

public class ASTAssign extends ASTBaseExpr {

	public final IASTExpression v;
	public final IASTExpression e;

	public ASTAssign(IASTExpression e, IASTExpression v) {
		this.v = v;
		this.e = e;
	}
}
