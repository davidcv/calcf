package ast.expr;

import ast.ASTBaseExpr;
import ast.expr.type.IType;

public class ASTId extends ASTBaseExpr {

	public final String id;

	public ASTId(String id) {
		this.id = id;
	}
	
	public ASTId(String id,IType t) {
		this.id = id;
		setType(t);
	}
}
