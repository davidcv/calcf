package ast.expr;

import ast.ASTBaseExpr;
import ast.IASTExpression;

public class ASTEq  extends ASTBaseExpr {

	public final String id;
	public final IASTExpression v;

	public ASTEq(String id, IASTExpression v) {
		this.id = id;
		this.v = v;
	}



}
