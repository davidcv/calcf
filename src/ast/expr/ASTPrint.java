package ast.expr;

import ast.ASTBaseExpr;
import ast.IASTExpression;

public class ASTPrint extends ASTBaseExpr{
	
	public final IASTExpression e;

	public ASTPrint(IASTExpression e) {
		this.e = e;
	
	}
}
