package ast.expr;

import ast.ASTBaseExpr;
import ast.IASTExpression;

public class ASTCall extends ASTBaseExpr {
	public final IASTExpression ee;
	public final IASTExpression e;

	public ASTCall(IASTExpression e, IASTExpression ee) {
		this.e = e;
		this.ee = ee;
	}
}
