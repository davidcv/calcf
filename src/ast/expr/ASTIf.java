package ast.expr;

import ast.ASTBaseExpr;
import ast.IASTExpression;


public class ASTIf extends ASTBaseExpr {

	public final IASTExpression cond;
	public final IASTExpression ifBody;
	public final IASTExpression elseBody;
	
	
	public ASTIf(IASTExpression cond, IASTExpression ifBody,IASTExpression elseBody) {
		this.cond = cond;
		this.ifBody = ifBody;
		this.elseBody = elseBody;
	}

	

}
