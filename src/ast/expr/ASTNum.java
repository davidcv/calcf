package ast.expr;

import ast.ASTBaseExpr;

public class ASTNum  extends ASTBaseExpr {

	public final int num;

	public ASTNum(int num) {
		this.num = num;
	}

	
}
