package ast.expr;

import ast.ASTBaseExpr;
import ast.IASTExpression;

public class ASTSeq extends ASTBaseExpr {
	
	public final IASTExpression b;
	public final IASTExpression a;

	public ASTSeq(IASTExpression a, IASTExpression b) {
		this.a = a;
		this.b = b;
		}

}
