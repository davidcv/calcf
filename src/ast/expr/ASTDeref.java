package ast.expr;

import ast.ASTBaseExpr;

public class ASTDeref  extends ASTBaseExpr{
	
	public final String id;

	public ASTDeref(String id) {
		this.id = id;
	}

}
