package ast.expr;

import ast.ASTBaseExpr;
import ast.IASTExpression;

public class ASTGt  extends ASTBaseExpr {

	public final IASTExpression l;
	public final IASTExpression r;

	public ASTGt(IASTExpression l, IASTExpression r) {
		this.l = l;
		this.r = r;

	}

}
