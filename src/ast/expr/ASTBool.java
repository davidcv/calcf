package ast.expr;

import ast.ASTBaseExpr;
import ast.expr.type.BoolT;
import ast.expr.type.IType;

public class ASTBool extends ASTBaseExpr {

	public final Boolean b;
	private final IType t = new BoolT();

	public ASTBool(Boolean b) {
		setType(t);
		this.b = b;
	}

	@Override
	public void setType(IType t) {
		// DO noting
	}
}
