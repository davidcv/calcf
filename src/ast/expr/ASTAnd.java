package ast.expr;

import ast.ASTBaseExpr;
import ast.IASTExpression;

public class ASTAnd extends ASTBaseExpr {

	public final IASTExpression l;
	public final IASTExpression r;

	public ASTAnd(IASTExpression l, IASTExpression r) {
		this.l = l;
		this.r = r;

	}

}
