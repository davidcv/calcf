package ast.expr;

import ast.ASTBaseExpr;
import ast.IASTExpression;

public class ASTNot  extends ASTBaseExpr {

	public final IASTExpression e;

	public ASTNot(IASTExpression e) {
		this.e = e;
	}

}
