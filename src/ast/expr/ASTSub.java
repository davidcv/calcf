package ast.expr;

import ast.ASTBaseExpr;
import ast.IASTExpression;

public class ASTSub extends ASTBaseExpr {

	public final IASTExpression l, r;

	public ASTSub(IASTExpression l, IASTExpression r) {
		this.l = l;
		this.r = r;
	}

	

}
