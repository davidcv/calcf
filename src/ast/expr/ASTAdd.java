package ast.expr;

import ast.ASTBaseExpr;
import ast.IASTExpression;

public class ASTAdd extends ASTBaseExpr {

	public final IASTExpression l, r;
	

	public ASTAdd(IASTExpression l, IASTExpression r) {
		this.l = l;
		this.r = r;
	}

	
}
