package ast.expr;

import ast.ASTBaseExpr;
import ast.IASTExpression;
import ast.expr.type.FunT;
import ast.expr.type.IType;

public class ASTFunc extends ASTBaseExpr {

	public final IASTExpression e;
	public final ASTId param;

	public ASTFunc(ASTId param, IASTExpression e) {
		this.param = param;
		this.e = e;
	}

	public ASTFunc(ASTId param, IType ty, IASTExpression e) {
		this.param = param;
		this.e = e;
	}

	@Override
	public IType getType() {
		IType t = this.param.getType();
		return new FunT(t, t);
	}

}
