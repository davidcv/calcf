package ast.expr;

import ast.ASTBaseExpr;
import ast.IASTExpression;

public class ASTDiv  extends ASTBaseExpr {

	public final IASTExpression l, r;

	public ASTDiv(IASTExpression l, IASTExpression r) {
		this.l = l;
		this.r = r;
	}

}
