

@.str = private unnamed_addr constant [5 x i8] c" %d \00", align 1

@.strln = private unnamed_addr constant [2 x i8] c"\0A\00", align 1
%struct.var_type = type { i32, %union.anon }
%union.anon = type { %struct.var_type* }
%struct.closure_type = type { i8*, void ()* }

define i32 @main() nounwind {

Entry:
  %0 = call %struct.var_type* @int_var_create(i32 0) nounwind ssp 
  br label %BB0

BB0:
  %1 = call i32 @int_get_var(%struct.var_type* %0) nounwind ssp 
  br label %BB5

BB5:
  %2 = icmp sge i32 10 , %1
  br label %BB1

BB2:
  %3 = call i32 @int_get_var(%struct.var_type* %0) nounwind ssp 
  br label %BB9

BB9:
  %4 = icmp sge i32 5 , %3
  br label %BB7

BB10:
  %5 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([5 x i8]* @.str, i32 0, i32 0), i32 1)
  br label %BB11

BB13:
  %6 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([5 x i8]* @.str, i32 0, i32 0), i32 0)
  br label %BB14

BB7:
  br i1 %4, label %BB10, label %BB13

BB11:
  br label %BB16

BB14:
  br label %BB16

BB16:
  %7 = phi i32 [%5 , %BB11], [%6 , %BB14]
  br label %BB6

BB6:
  %8 = call i32 @int_get_var(%struct.var_type* %0) nounwind ssp 
  br label %BB18

BB18:
  %9 = add i32 %8 , 1
  br label %BB17

BB17:
  call void @int_set_var(%struct.var_type* %0, i32 %9) nounwind ssp
  br label %BB3

BB1:
  br i1 %2, label %BB2, label %RESULT

BB3:
  br label %BB0

RESULT:
  ret i32 0
}

declare i32 @printf(i8*, ...) nounwind
declare noalias i8* @malloc(i64) nounwind
declare %struct.var_type* @int_var_create(i32)
declare %struct.var_type* @var_var_create(%struct.var_type*)
declare i32 @int_get_var(%struct.var_type*)
declare %struct.var_type* @var_get_var(%struct.var_type*)
declare void @int_set_var(%struct.var_type*, i32)
declare void @var_set_var(%struct.var_type*, %struct.var_type*)
declare void @free_var(%struct.var_type*)
declare %struct.closure_type* @closure_create(i8*, void ()*)
